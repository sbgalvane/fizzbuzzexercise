package fizzBuzz;

public class FizzBuzzPrinter {

	public static String fizzBuzzPrinter(Integer i) {
		boolean checkFizz = FizzBuz.checkFizz(i);
		boolean checkBuzz = FizzBuz.checkBuzz(i);
		boolean checkFizzBuzz = FizzBuz.checkFizzBuzz(i);
		if ( checkFizzBuzz ) {
			return "FizzBuzz";
		} else if ( checkFizz ) {
			return "Fizz";
		}else if ( checkBuzz ) {
			return "Buzz";
		}
		return i.toString();
	}
}
